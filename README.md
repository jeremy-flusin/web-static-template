# web-static-template

SEO friendly HTML static template

# Bonnes pratiques

- Forker ce projet et ne pas développer de site dedans.
- Le style se développe dans le répertoire scss/ et est transpilé en css via `npm run sass`

## Commandes

`npm run dev` : lance un serveur de dev  
`npm run new:page -- page-name` : génère une page 'page-name' et tous les fichiers associés (scss...)  
`npm run build`: build les sources du projet dans le répertoire dist/  
`npm run serve` : lance un server http qui watch les sources du projet. Utilisé par `npm run dev`.  
`npm run sass` : transpile les sources scss en css en watching. Utilisé par `npm run dev`.  
`npm run sass:build` : transpile les sources scss en css sans watching. Utilisé par `npm run build`.  
