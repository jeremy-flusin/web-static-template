#!/bin/bash
set -e

echo "[INFO] Create dist directory"
rm -rf dist/
mkdir dist/

echo "[INFO] Copy HTML sources"
cp *.html dist/

echo "[INFO] Copy assets"
cp -R assets dist/

echo "[INFO] Copy JS sources"
cp -R js dist/

echo "[INFO] Build CSS sources"
npm run sass:build

echo "[INFO] Build done."
