#!/bin/bash
set -e

if test -f "$1.html"; then
    echo "[ERROR] $1 page already exists, exiting."
    exit 1
fi

echo "[INFO] Creating page $1"

cp scripts/page_template.html $1.html
sed -i "s/Title/$1 page/g" $1.html
sed -i "s/CSS_FILE/$1.css/g" $1.html
sed -i "s/JS_FILE/$1.js/g" $1.html
sed -i "s/DUMMY_CONTENT/$1 page works !/g" $1.html

mkdir -p scss/pages/$1
touch scss/pages/$1.scss

touch scss/pages/$1/$1.main.scss
echo "@import '../../vars.scss';" >> scss/pages/$1/$1.main.scss
echo "/* Specific CSS for page '$1.html' goes here */" >> scss/pages/$1/$1.main.scss
touch scss/pages/$1/$1.mq.scss
echo "@import '../../vars.scss';" >> scss/pages/$1/$1.mq.scss
echo "/* Specific Media-Queries for page '$1.html' goes here */" >> scss/pages/$1/$1.mq.scss

echo "@import '../reset.scss';" >> scss/pages/$1.scss
echo "@import '../fonts.scss';" >> scss/pages/$1.scss
echo "@import '../common.scss';" >> scss/pages/$1.scss
echo "@import '$1/$1.main.scss';" >> scss/pages/$1.scss
echo "@import '$1/$1.mq.scss';" >> scss/pages/$1.scss

touch js/$1.js
echo "// JS for page $1.html goes here" >> js/$1.js

echo "[INFO] Page $1 created"
